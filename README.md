# U-BootMemBitman
U-BootMemBitman is a small program that helps you manipulate bits in a device's memory through its U-Boot.  
It acts as a shell with a few commands that make flipping bits quick and easy.
It's useful for GPIO testing, among other things.

### Installation
- git clone https://gitlab.com/ar2000jp/U-BootMemBitman.git
- cd U-BootMemBitman
- python3 -mvenv venv
- source ./venv/bin/activate
- pip install pyserial

### Usage
- ./U-BootMemBitman.py 0x10100000 -p "odroidc#"
- Type "h" to see the list of commands

### License
Copyright 2018 Ahmad Draidi and the U-BootMemBitman contributors.  
SPDX-License-Identifier: GPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
