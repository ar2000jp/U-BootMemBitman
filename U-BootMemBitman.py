#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#
# Copyright 2018,
# Ahmad Draidi and the U-BootMemBitman contributors
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import re
import sys
import time
import serial

class Bitman():
    def __init__(self):
        self.ttyBaudrate = 115200
        self.ttyDevName = "/dev/ttyUSB0"
        self.ttyDev = None

        self.memOpBaseAddr = 0x18040000
        self.memOpSize = 4

        # Used to check if U-Boot has finished executing a command.
        self.uBootPrompt = "U-Boot#"
        self.uBootMemReadCmd = "md"
        self.uBootMemWriteCmd = "mw"
        self.uBootMemCmdSuffix = ".l"
        # Used to check if U-Boot is responding.
        self.uBootNopCmd = " "

    def __del__(self):
        self.atExit()

    def atExit(self):
        try:
            self.ttyDev.close()
        except Exception:
            pass

    def uBootRun(self, cmd):
        cmd += bytes("\n", "ascii")
        print("Executing:", cmd)
        self.ttyDev.write(cmd)

        buf = bytes()
        tmpBuf = bytes()
        while True:
            tmpBuf = self.ttyDev.read(self.ttyDev.inWaiting() + 16)
            if tmpBuf == bytes():
                break
            buf += tmpBuf

        # Normalize newlines
        buf = buf.replace(bytes("\r\r\n", "ascii"), bytes("\n", "ascii"))
        buf = buf.replace(bytes("\r\n", "ascii"), bytes("\n", "ascii"))
        
        # Remove echoed cmd
        bufSlice = buf.rpartition(cmd)
        bufSlice = bufSlice[2]

        bufLines = buf.splitlines()
        # Check if the last line is a U-Boot prompt.
        if len(bufLines) < 1 or bytes(self.uBootPrompt, "ascii") not in bufLines[-1]:
            print("U-Boot not responding correctly.")
            sys.exit(1)

        return bufSlice

    def main(self):
        print("U-BootMemBitman: Memory bits manipulation for U-Boot.")
        print("Copyright 2018,")
        print("Ahmad Draidi and the U-BootMemBitman contributors.\n")

        parser = argparse.ArgumentParser(description="Manipulate memory bits "
                                         "through the U-Boot shell.")
        parser.add_argument("address", help="Base address of memory to manipulate.",
                            metavar="BaseAddress")
        parser.add_argument("-s", "--size", help="Number of bytes to act on (word size)."
        " Can be either 1, 2 or 4. Default is {0:d} bytes.".format(
        self.memOpSize), metavar="Bytes")
        parser.add_argument("-b", "--baudrate", help="TTY baudrate to use."
                        "Default is {0:d}.".format(self.ttyBaudrate),
                        metavar="Baudrate")
        parser.add_argument("-d", "--ttydevice", help="TTY device to use."
                        " Default is \"{0:s}\".".format(self.ttyDevName),
                        metavar="Device")
        parser.add_argument("-p", "--prompt", help="U-Boot shell prompt."
                        " Default is \"{0:s}\".".format(self.uBootPrompt),
                        metavar="PromptString")
        args = parser.parse_args()

        try:
            self.memOpBaseAddr = int(args.address, 0)
        except ValueError:
            print("Invalid base address.")
            sys.exit(1)

        if args.prompt != None:
            self.uBootPrompt = args.prompt

        if args.ttydevice != None:
            self.ttyDevName = args.ttydevice

        if args.baudrate != None:
            try:
                self.ttyBaudrate = int(args.baudrate, 0)
            except ValueError:
                print("Invalid baudrate.")
                sys.exit(1)

        if args.size != None:
            try:
                self.memOpSize = int(args.size, 0)
            except ValueError:
                print("Invalid word size.")
                sys.exit(1)

            if self.memOpSize == 1:
                self.uBootMemCmdSuffix = ".b"
            elif self.memOpSize == 2:
                self.uBootMemCmdSuffix = ".w"
            elif self.memOpSize == 4:
                self.uBootMemCmdSuffix = ".l"
            else:
                print("Invalid word size. 1, 2 or 4 only allowed.")
                sys.exit(1)

        try:
            self.ttyDev = serial.Serial(self.ttyDevName, self.ttyBaudrate,
            timeout=1)
        except serial.SerialException as e:
            print(str(e))
            sys.exit(1)

        ttySettingsDict = self.ttyDev.getSettingsDict()
        # I guess we need to wait after opening TTY in some cases. I don't remember anymore.
        time.sleep(1)

        self.ttyDev.flushInput()
        self.ttyDev.flushOutput()

        ttySettingsDict["timeout"] = 0.5
        self.ttyDev.applySettingsDict(ttySettingsDict)

        # Check if we're connected to U-Boot.
        for i in range(2):
            buf = self.uBootRun(bytes(self.uBootNopCmd, "ascii"))
            if bytes(self.uBootPrompt, "ascii") in buf:
                print("U-Boot responding.")
            else:
                print("U-Boot not responding.")
                sys.exit(1)
        print()

        # Use shorter timeout between reads
        ttySettingsDict["timeout"] = 0.01
        self.ttyDev.applySettingsDict(ttySettingsDict)

        print("Base address: 0x{0:08X},".format(int(self.memOpBaseAddr)),
            "word size: {0:d} bytes.".format(self.memOpSize))

        quitLoop = False
        addrOffset = 0
        curVal = None
        newVal = None

        while not quitLoop:
            curAddr = self.memOpBaseAddr + addrOffset

            buf = self.uBootRun(bytes(self.uBootMemReadCmd + self.uBootMemCmdSuffix +
                " 0x{0:08X} 0x{1:X}".format(curAddr, 1), "ascii"))
            bufLines = buf.splitlines()

            for line in bufLines:
                # Skip lines that don't have an address in them. Addresses end with ":".
                if((line == bytes()) or (bytes(":", "ascii") not in line)):
                    continue
                linePieces = line.split()

                # Sanity check. First part must contain address.
                if(linePieces[0].endswith(bytes(":", "ascii")) is False):
                    print("Invalid line structure. Probably an IO error.")
                    sys.exit(1)

                # Sanity check. Calculated address must be equal to read address.
                if(int(linePieces[0][:-1], 16) != curAddr):
                    print("Error: Calculated Address != Read Address."
                        " Probably an IO error.")
                    sys.exit(1)

            try:
                curVal = int(linePieces[1], 16)
            except ValueError:
                print("Invalid memory value. Probably an IO error.")
                sys.exit(1)
            newVal = curVal

            curValHexStr = "0x" + "{0:X}".format(curVal).zfill(2 * self.memOpSize)
            curValBinStr = "0b" + " ".join(re.compile(".{4}").findall(
                                      "{0:b}".format(curVal).zfill(
                                      8 * self.memOpSize)))
            print("Address: 0x{0:08X}".format(curAddr),
            "Hex value: " + curValHexStr + ", binary value: " + curValBinStr)

            op = input("Enter op. code (h for help): ")
            if not op:
                pass
            elif op[0] == "h":
                print("h")
                print("\tShow this help message.")
                print("s value")
                print("\tSet memory word to value.")
                print("Example: \"s0xff00ff00\"")
                print("c")
                print("\tClear memory word (set all bits to zeros).")
                print("i")
                print("\tInvert memory word (flip all bits).")
                print("f bit_number")
                print("\tFlip bit at index specified by bit_number (starts from zero).")
                print("\tExample: \"f1\"")
                print("x xor_value")
                print("\tFlips bits set as 1 in xor_value (i.e. mem ^ xor_value).")
                print("\tExamples: \"x0xB\", \"x11\"")
                print("n")
                print("\tIncrease offset by 1 memory word size (set by -s parameter).")
                print("p")
                print("\tDecrease offset by 1 memory word size (set by -s parameter).")
                print("o [offset]")
                print("\tChange offset from base address for memory operations.")
                print("\tExamples: \"o0x8\", \"o24\"")
                print("b [address]")
                print("\tChange base address for memory operations.")
                print("\tExamples: \"b0x18040000\", \"o32768\"")
                print("q")
                print("\tQuit.")
                print("")
            elif op[0] == "s":
                try:
                    newVal = int(op[1:], 0)
                except ValueError:
                    print("Invalid value.")
            elif op[0] == "c":
                newVal = 0
            elif op[0] == "i":
                newVal = curVal ^ (0xFFFFFFFF >> ((4 - self.memOpSize) * 8))
            elif op[0] == "f":
                try:
                    newVal = curVal ^ (1 << int(op[1:], 0))
                except ValueError:
                    print("Invalid value.")
            elif op[0] == "x":
                try:
                    newVal = curVal ^ int(op[1:], 0)
                except ValueError:
                    print("Invalid value.")
            elif op[0] == "n":
                addrOffset += self.memOpSize
            elif op[0] == "p":
                addrOffset -= self.memOpSize
            elif op[0] == "o":
                try:
                    if len(op) == 1:
                        o = int(input("Enter address offset(0x{0:08X}): "
                             .format(addrOffset)), 0)                             
                    else:
                        o = int(op[1:], 0)
                    
                    if o % self.memOpSize != 0:
                        print("Offset must be multiples of memory word size (set by -s).")
                    else:
                        addrOffset = o
                except ValueError:
                        print("Invalid value.")
            elif op[0] == "b":
                try:
                    if len(op) == 1:
                        b = int(input("Enter base address(0x{0:08X}): "
                             .format(self.memOpBaseAddr)), 0)
                    else:
                        b = int(op[1:], 0)

                    if b % self.memOpSize != 0:
                        print("Address must be multiples of memory word size (set by -s).")
                    else:    
                        self.memOpBaseAddr = b
                except ValueError:
                    print("Invalid value.")
            elif op[0] == "q":
                quitLoop = True
            else:
                print("Invalid input.")

            if newVal != curVal:
                newValHexStr = "0x" + "{0:X}".format(newVal).zfill(2 * self.memOpSize)
                self.uBootRun(bytes(self.uBootMemWriteCmd + self.uBootMemCmdSuffix +
                       " 0x{0:08X} ".format(curAddr) + newValHexStr,
                       "ascii"))
                newVal = curVal

            print()

if __name__ == "__main__":
    bman = Bitman()
    sys.exitfunc = bman.atExit
    bman.main()
    print("Bye!")
